# HSRのデバイスを使う

## 外付けスピーカーを使う[(https://docs.hsr.io/hsr_faq/hsr_common_faq.html#id6)]
外付けのUSBスピーカを使うための設定

以下の手順で設定すれば、デフォルトの出力先が、外付けのUSBスピーカになり、一度設定すれば、再起動毎に再設定する必要はない。

- USBスピーカーを接続する
- 出力デバイスを調べる
```
$ pactl list  | grep alsa_output
Failed to load cookie file from cookie: No such file or directory
    Name: alsa_output.pci-0000_00_03.0.hdmi-stereo
    Monitor Source: alsa_output.pci-0000_00_03.0.hdmi-stereo.monitor
    Name: alsa_output.pci-0000_00_1b.0.analog-stereo
    Monitor Source: alsa_output.pci-0000_00_1b.0.analog-stereo.monitor
    Name: alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo
    Monitor Source: alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo.monitor
    Name: alsa_output.pci-0000_00_03.0.hdmi-stereo.monitor
    Monitor of Sink: alsa_output.pci-0000_00_03.0.hdmi-stereo
    Name: alsa_output.pci-0000_00_1b.0.analog-stereo.monitor
    Monitor of Sink: alsa_output.pci-0000_00_1b.0.analog-stereo
    Name: alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo.monitor
    Monitor of Sink: alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo
```
- /etc/pulse/system.paの編集
/etc/pulse/system.pa に調べたデバイスを以下のように記載する このとき、もともとあった記述（ set-default-sink 、 set-sink-volume ）はコメントアウトする
```
set-default-sink alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo
set-sink-volume alsa_output.usb-C-Media_Electronics_Inc._USB_Audio_Device-00.analog-stereo 65536
```

- ロボットを再起動する

## [スピーカーの音量を調整する](https://docs.hsr.io/hsr_faq/hsr_common_faq.html#id7)
HSR内のファイル( /etc/pulse/system.pa ) の set-sink-volume を変更することで、音量の調整ができる。

- ボリュームを以下の様に100000に設定する
```
set-sink-volume alsa_output.pci-0000_00_1b.0.analog-stereo 100000
```

- ロボットを再起動する


##　HSRの頭のマイクを使う
### HSR-B
```
$ cat /proc/asound/cards 
 0 [CameraB409241  ]: USB-Audio - USB Camera-B4.09.24.1
                      OmniVision Technologies, Inc. USB Camera-B4.09.24.1 at usb-0000:02:00.0-1.2, hi
 1 [Device         ]: USB-Audio - PrimeSense Device
                      PrimeSense PrimeSense Device at usb-0000:00:1d.0-1.1.4, high speed
 2 [HDMI           ]: HDA-Intel - HDA Intel HDMI
                      HDA Intel HDMI at 0xf7e24000 irq 58
 3 [PCH            ]: HDA-Intel - HDA Intel PCH
                      HDA Intel PCH at 0xf7e20000 irq 59
```

```
PyAudio().get_device_info_by_id() で認識されているのは

(0, u'USB Camera-B4.09.24.1: Audio (hw:0,0)')
(1, u'PrimeSense Device: USB Audio (hw:1,0)')
(2, u'HDA Intel HDMI: 0 (hw:2,3)')
(3, u'HDA Intel HDMI: 1 (hw:2,7)')
(4, u'HDA Intel HDMI: 2 (hw:2,8)')
(5, u'HDA Intel PCH: CS4207 Analog (hw:3,0)')
(6, u'HDA Intel PCH: CS4207 Digital (hw:3,1)')
(7, u'sysdefault')
(8, u'spdif')
(9, u'pulse')
(10, u'default')
```

これがPS3Eye
```
 　 id:0 USB Camera-B4.09.24.1
```
alsaからは`plughw:0,0`

Xtionはマイクid:1で認識

- Xtionは感度が高く，ノイズも多い
- PS3Eyeはあまり感度は良くないが，雑音も拾いづらい

### HSR-C
